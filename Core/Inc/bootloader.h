#ifndef _bootloader_H_
#define _bootloader_H_


typedef void (*pFunction)(void);



void vJUMPInit(void);

int8_t cJUMPToFirmWare(uint32_t uiAddress);

int8_t cJUMPSetAPPSwitch(uint8_t ucState);
int8_t cJUMPSetBootloaderSwitch(uint8_t ucState);


#endif
