/*
 * DevicesSPIFlash.h
 *
 *  Created on: 2020��10��20��
 *      Author: hongyun.he
 */

#ifndef DEVICESSPIFLASH_H_
#define DEVICESSPIFLASH_H_





#define SPI_FLASH_APP_ADDR                        (ADDR_FLASH_SECTOR_5 - ADDR_FLASH_SECTOR_0)
#define SPI_FLASH_BOOTLOADER_ADDR                 (ADDR_FLASH_SECTOR_3 - ADDR_FLASH_SECTOR_0)








/* Reset Operations */
#define RESET_ENABLE_CMD                     0x66
#define RESET_MEMORY_CMD                     0x99

/* Identification Operations */
#define READ_ID_CMD                          0x9E
#define READ_ID_CMD2                         0x9F
#define MULTIPLE_IO_READ_ID_CMD              0xAF
#define READ_SERIAL_FLASH_DISCO_PARAM_CMD    0x5A

/* Read Operations */
#define READ_CMD                             0x03
#define FAST_READ_CMD                        0x0B
#define DUAL_OUT_FAST_READ_CMD               0x3B
#define DUAL_INOUT_FAST_READ_CMD             0xBB
#define QUAD_OUT_FAST_READ_CMD               0x6B
#define QUAD_INOUT_FAST_READ_CMD             0xEB

/* Write Operations */
#define WRITE_ENABLE_CMD                     0x06
#define WRITE_DISABLE_CMD                    0x04

/* Register Operations */
#define READ_STATUS_REG_CMD                  0x05
#define WRITE_STATUS_REG_CMD                 0x01

#define READ_LOCK_REG_CMD                    0xE8
#define WRITE_LOCK_REG_CMD                   0xE5

#define READ_FLAG_STATUS_REG_CMD             0x70
#define CLEAR_FLAG_STATUS_REG_CMD            0x50

#define READ_NONVOL_CFG_REG_CMD              0xB5
#define WRITE_NONVOL_CFG_REG_CMD             0xB1

#define READ_VOL_CFG_REG_CMD                 0x85
#define WRITE_VOL_CFG_REG_CMD                0x81

#define READ_ENHANCED_VOL_CFG_REG_CMD        0x65
#define WRITE_ENHANCED_VOL_CFG_REG_CMD       0x61

/* Program Operations */
#define PAGE_PROG_CMD                        0x02
#define DUAL_IN_FAST_PROG_CMD                0xA2
#define EXT_DUAL_IN_FAST_PROG_CMD            0xD2
#define QUAD_IN_FAST_PROG_CMD                0x32
#define EXT_QUAD_IN_FAST_PROG_CMD            0x12

/* Erase Operations */
#define SUBSECTOR_ERASE_CMD                  0x20
#define SECTOR_ERASE_CMD                     0xD8
#define BULK_ERASE_CMD                       0xC7

#define PROG_ERASE_RESUME_CMD                0x7A
#define PROG_ERASE_SUSPEND_CMD               0x75

/* One-Time Programmable Operations */
#define READ_OTP_ARRAY_CMD                   0x4B
#define PROG_OTP_ARRAY_CMD                   0x42



void vSPIFlashInit(void);
int8_t cSPIFlashErases(uint32_t uiAddress);
int8_t cSPIFlashWritePage(uint32_t uiAddress, uint8_t *pucDatas, uint32_t uiLength);
int8_t cSPIFlashWriteDatas(uint32_t uiAddress, uint8_t *pucDatas, uint32_t iLength);
int8_t cSPIFlashReadDatas(uint32_t uiAddress, uint8_t *pucDatas, uint32_t uiLength);
uint32_t uiSPIFlashReadID(void);
uint8_t ucSPIFlashReadStatus(uint32_t uiFlag);

void uvSPIFlashEnableWrite(void);

#endif /* DEVICESSPIFLASH_H_ */
