#ifndef _DevicesLed_H_
#define _DevicesLed_H_


#define LED_0   0x01
#define LED_1   0x02
#define LED_2   0x04
#define LED_3   0x08
#define LED_ALL 0xFF



#define LED_RCC_GPIO_CLK_ENABLE() __HAL_RCC_GPIOE_CLK_ENABLE()
#define LED_GPIO_Port             GPIOE
#define LED_Pin                   (GPIO_PIN_12 | GPIO_PIN_15)




void vLedInit(void);
void vLedOpen(uint8_t ucNumber);
void vLedClose(uint8_t ucNumber);
void vLedRevesal(uint8_t ucNumber);


#define LED_OPEN()      vLedOpen(LED_ALL)
#define LED_CLOSE()     vLedClose(LED_ALL)
#define LED_REVESAL()   vLedRevesal(LED_ALL)


#endif
