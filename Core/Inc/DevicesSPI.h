/*
 * DevicesSPI.h
 *
 *  Created on: 2020��10��20��
 *      Author: hongyun.he
 */

#ifndef DEVICESSPI_H_
#define DEVICESSPI_H_



extern SPI_HandleTypeDef ex_Spi1Handle;
extern SPI_HandleTypeDef ex_Spi2Handle;


#define SPI2_NSS_ENABLE() HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET)
#define SPI2_NSS_DISABLE() HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET)





void vSPI2Init(void);

int8_t cSPIWriteDatas(SPI_HandleTypeDef *hSPIHand, uint8_t *pBuffer, uint32_t uiLength);
int8_t cSPIReadDatas(SPI_HandleTypeDef *hSPIHand, uint8_t *pBuffer, uint32_t uiLength);

#define cSPI2WriteDatas(pBuffer, uiLength) cSPIWriteDatas(&ex_Spi2Handle, (pBuffer), (uiLength))
#define cSPI2ReadDatas(pBuffer, uiLength) cSPIReadDatas(&ex_Spi2Handle, (pBuffer), (uiLength))


#endif /* DEVICESSPI_H_ */
