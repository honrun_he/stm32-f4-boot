/*
 * DevicesUart.h
 *
 *  Created on: 2019��12��17��
 *      Author: hongyun.he
 */

#ifndef DEVICESUART_H_
#define DEVICESUART_H_

#include "stm32f4xx_hal.h"
#include "Version.h"

#define USART1_DMA_READ_LENGTH  (1024 * 4)
#define USART2_DMA_READ_LENGTH  (1024 * 4)



extern UART_HandleTypeDef ex_Uart2Handle;


extern DMA_HandleTypeDef ex_Uart2DmaTx;
extern DMA_HandleTypeDef ex_Uart2DmaRx;


extern uint8_t ex_USART2ReadDMABuff[USART2_DMA_READ_LENGTH + 4];



void vUart1Init(void);
void vUart2Init(void);

void vUart1DMAInit(UART_HandleTypeDef *huart);
void vUart2DMAInit(UART_HandleTypeDef *huart);


void vDMASetLenthStart(DMA_HandleTypeDef *hdma, uint32_t uiLength);
void vDMAMultiBufferSetLenthStart(DMA_HandleTypeDef *hdma, uint32_t uiLength);


void vUartSendStrings(USART_TypeDef* USARTx, char *pcStrings);
void vUartSendDatas(USART_TypeDef* USARTx, uint8_t *pucDatas, uint32_t uiLength);


void vUartDMASendStrings(USART_TypeDef* USARTx, char *pcStrings);
void vUartDMASendDatas(USART_TypeDef* USARTx, uint8_t *pucDatas, uint32_t uiLength);


#endif /* DEVICESUART_H_ */
