/*
 * UserOTA.h
 *
 *  Created on: 2020��9��15��
 *      Author: Honrun
 */

#ifndef USEROTA_H_
#define USEROTA_H_



#define OTA_TOTAL_LENGTH_MAX (2 * 1024 * 1024)
#define OTA_TOTAL_LENGTH_MIN 8




typedef struct{
    uint32_t jumpSwitch;

    uint32_t startAddr;
    uint32_t totalLength;
    uint32_t totalCRC;

    char version[16];

    uint8_t reserved[512 - (sizeof(uint32_t) * 4 + sizeof(char) * 16)];
}OTAFirmware;


extern OTAFirmware ex_firmwaeMCUAPP, ex_firmwaeMCUBootloader, ex_firmwaeMCUAPPUpdate, ex_firmwaeMCUBootloaderUpdate;



int8_t cOTAParametersRead(void);
int8_t cOTAParametersWrite(void);
int8_t cOTAUpdate(OTAFirmware *pSrc, OTAFirmware *pDst);
uint32_t uiOTAFirmwaeCRCUpdate(OTAFirmware *pSrc);


#endif /* USEROTA_H_ */
