#ifndef _DEVICES_DELAY_H_
#define _DEVICES_DELAY_H_


#define  DWT_CR      *(__IO uint32_t *)0xE0001000
#define  DWT_CYCCNT  *(__IO uint32_t *)0xE0001004
#define  DEM_CR      *(__IO uint32_t *)0xE000EDFC


#define  DEM_CR_TRCENA                   (1 << 24)
#define  DWT_CR_CYCCNTENA                (1 <<  0)


#define vDelayInit  vDWTDelayInit
#define vDelayS     vDWTDelayS
#define vDelayMs    vDWTDelayMs
#define vDelayUs    vDWTDelayUs


void vSysTickDelayInit(void);
void vSysTickDelayS(uint32_t uiTime);
void vSysTickDelayMs(float fTime);
void vSysTickDelayUs(float fTime);

void vDWTDelayInit(void);
void vDWTDelayS(uint32_t uiTime);
void vDWTDelayMs(float fTime);
void vDWTDelayUs(float fTime);

#endif
