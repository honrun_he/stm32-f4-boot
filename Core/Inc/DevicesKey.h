#ifndef _DevicesKey_H_
#define _DevicesKey_H_




#define KEY_0   0x01
#define KEY_1   0x02
#define KEY_2   0x04
#define KEY_3   0x08



#define KEY0_RCC_GPIO_CLK_ENABLE()   __HAL_RCC_GPIOE_CLK_ENABLE()
#define KEY0_GPIO_Port               GPIOE
#define KEY0_Pin                     GPIO_PIN_2

#define KEY1_RCC_GPIO_CLK_ENABLE()   __HAL_RCC_GPIOB_CLK_ENABLE()
#define KEY1_GPIO_Port               GPIOB
#define KEY1_Pin                     GPIO_PIN_3

#define KEY2_RCC_GPIO_CLK_ENABLE()   __HAL_RCC_GPIOB_CLK_ENABLE()
#define KEY2_GPIO_Port               GPIOB
#define KEY2_Pin                     GPIO_PIN_11

#define KEY3_RCC_GPIO_CLK_ENABLE()   __HAL_RCC_GPIOD_CLK_ENABLE()
#define KEY3_GPIO_Port               GPIOD
#define KEY3_Pin                     GPIO_PIN_8




void vKeyInit(void);
uint32_t uiKeyScanf(void);

#endif
