#ifndef _DevicesFlash_H_
#define _DevicesFlash_H_


/* Base address of the Flash sectors */
#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base address of Sector 0, 16 Kbytes   */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base address of Sector 1, 16 Kbytes   */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base address of Sector 2, 16 Kbytes   */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base address of Sector 3, 16 Kbytes   */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base address of Sector 4, 64 Kbytes   */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base address of Sector 5, 128 Kbytes  */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base address of Sector 6, 128 Kbytes  */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base address of Sector 7, 128 Kbytes  */
#define ADDR_FLASH_SECTOR_8     ((uint32_t)0x08080000) /* Base address of Sector 8, 128 Kbytes  */
#define ADDR_FLASH_SECTOR_9     ((uint32_t)0x080A0000) /* Base address of Sector 9, 128 Kbytes  */
#define ADDR_FLASH_SECTOR_10    ((uint32_t)0x080C0000) /* Base address of Sector 10, 128 Kbytes */
#define ADDR_FLASH_SECTOR_11    ((uint32_t)0x080E0000) /* Base address of Sector 11, 128 Kbytes */

#if defined (STM32F427_437xx)
#define ADDR_FLASH_SECTOR_12     ((uint32_t)0x08100000) /* Base address of Sector 12, 16 Kbytes  */
#define ADDR_FLASH_SECTOR_13     ((uint32_t)0x08104000) /* Base address of Sector 13, 16 Kbytes  */
#define ADDR_FLASH_SECTOR_14     ((uint32_t)0x08108000) /* Base address of Sector 14, 16 Kbytes  */
#define ADDR_FLASH_SECTOR_15     ((uint32_t)0x0810C000) /* Base address of Sector 15, 16 Kbytes  */
#define ADDR_FLASH_SECTOR_16     ((uint32_t)0x08110000) /* Base address of Sector 16, 64 Kbytes  */
#define ADDR_FLASH_SECTOR_17     ((uint32_t)0x08120000) /* Base address of Sector 17, 128 Kbytes */
#define ADDR_FLASH_SECTOR_18     ((uint32_t)0x08140000) /* Base address of Sector 18, 128 Kbytes */
#define ADDR_FLASH_SECTOR_19     ((uint32_t)0x08160000) /* Base address of Sector 19, 128 Kbytes */
#define ADDR_FLASH_SECTOR_20     ((uint32_t)0x08180000) /* Base address of Sector 20, 128 Kbytes */
#define ADDR_FLASH_SECTOR_21     ((uint32_t)0x081A0000) /* Base address of Sector 21, 128 Kbytes */
#define ADDR_FLASH_SECTOR_22     ((uint32_t)0x081C0000) /* Base address of Sector 22, 128 Kbytes */
#define ADDR_FLASH_SECTOR_23     ((uint32_t)0x081E0000) /* Base address of Sector 23, 128 Kbytes */
#endif /* USE_STM324x7I_EVAL or USE_STM324x9I_EVAL */





#define FLASH_USER_MAX_ADDR             ((uint32_t)0x08100000)
#define FLASH_ZERO_ADDR                 ((uint32_t)0x08000000)


#define BOOT0_ADDR                      FLASH_ZERO_ADDR

#define BOOTLOADER_ADDR                 (FLASH_ZERO_ADDR + (16 * 1024 * 3))
#define BOOTLOADER_PARAMETERS_ADDR      (FLASH_ZERO_ADDR + (16 * 1024 * 1))
#define APP_PARAMETERS_ADDR             (FLASH_ZERO_ADDR + (16 * 1024 * 1) + (1 * 1024))

#define APP_ADDR                        (FLASH_ZERO_ADDR + (16 * 1024 * 4) + (64 * 1024 * 1))
#define APP_DATA_ADDR                   (FLASH_ZERO_ADDR + (16 * 1024 * 2))

#define BOOTLOADER_BACK_ADDR            (BOOT0_ADDR + (512 * 1024))
#define APP_BACK_ADDR                   (APP_ADDR + (512 * 1024))





int8_t cFlashWriteDatas(uint32_t uiAddress, uint8_t * pucBuff, uint32_t uiLength);
int8_t cFlashReadDatas(uint32_t uiAddress, uint8_t * pucBuff, uint32_t uiLength);

uint32_t uiFlashGetSector(uint32_t Address);

int8_t cFlashGetErasePosition(uint32_t Address);
uint32_t uiFlashGetPositionSize(uint32_t Address);
int8_t cFlashReadProtection(uint8_t NewState);


#endif
