/*
 * DevicesUart.c
 *
 *  Created on: 2019年12月17日
 *      Author: hongyun.he
 */
#include "stm32f4xx_hal.h"
#include "stdio.h"
#include "string.h"
#include "Version.h"
#include "DevicesDelay.h"
#include "DevicesUart.h"


UART_HandleTypeDef ex_Uart2Handle = {0};


DMA_HandleTypeDef ex_Uart2DmaTx = {0};
DMA_HandleTypeDef ex_Uart2DmaRx = {0};


uint8_t ex_USART2ReadDMABuff[USART2_DMA_READ_LENGTH + 4] = {0};


void vUart2Init(void)
{
    GPIO_InitTypeDef  GPIO_InitStruct = {0};

    /*##-1- Enable peripherals and GPIO Clocks #################################*/
    /* Enable GPIO TX/RX clock */
    __HAL_RCC_GPIOD_CLK_ENABLE();

    /* Enable USARTx clock */
    __HAL_RCC_USART2_CLK_ENABLE();

    /*##-2- Configure peripheral GPIO ##########################################*/
    /* UART TX/RX GPIO pin configuration  */
    GPIO_InitStruct.Pin       = GPIO_PIN_5 | GPIO_PIN_6;
    GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull      = GPIO_PULLUP;
    GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

    ex_Uart2Handle.Instance          = USART2;
    ex_Uart2Handle.Init.BaudRate     = 921600;
    ex_Uart2Handle.Init.WordLength   = UART_WORDLENGTH_8B;
    ex_Uart2Handle.Init.StopBits     = UART_STOPBITS_1;
    ex_Uart2Handle.Init.Parity       = UART_PARITY_NONE;
    ex_Uart2Handle.Init.HwFlowCtl    = UART_HWCONTROL_NONE;
    ex_Uart2Handle.Init.Mode         = UART_MODE_TX_RX;
    ex_Uart2Handle.Init.OverSampling = UART_OVERSAMPLING_16;
    HAL_UART_Init(&ex_Uart2Handle);

    vUart2DMAInit(&ex_Uart2Handle);

    /* Disable the UART Transmit Complete Interrupt */
    __HAL_UART_DISABLE_IT(&ex_Uart2Handle, UART_IT_IDLE);

    /* NVIC for USART, to catch the RX complete */
    HAL_NVIC_SetPriority(USART2_IRQn, 9, 0);
    HAL_NVIC_EnableIRQ(USART2_IRQn);

    /* Enable the UART transmit DMA stream */
    HAL_DMA_Start(&ex_Uart2DmaRx, (uint32_t)(&(USART2->DR)), (uint32_t)ex_USART2ReadDMABuff, USART2_DMA_READ_LENGTH);
    /* Enable the DMA transfer for transmit request by setting the DMAT bit in the UART CR3 register */
    SET_BIT(USART2->CR3, USART_CR3_DMAR);

//    /* Enable the UART transmit DMA stream */
//    HAL_DMA_Start(&ex_Uart2DmaTx, (uint32_t)ex_USART2ReadDMABuff, (uint32_t)(&(USART2->DR)), USART2_DMA_READ_LENGTH);
    /* Enable the DMA transfer for transmit request by setting the DMAT bit in the UART CR3 register */
    SET_BIT(USART2->CR3, USART_CR3_DMAT);
}

/**
  * @brief UART MSP Initialization
  *        This function configures the hardware resources used in this example:
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration
  *           - DMA configuration for transmission request by peripheral
  *           - NVIC configuration for DMA interrupt request enable
  * @param huart: UART handle pointer
  * @retval None
  */
void vUart2DMAInit(UART_HandleTypeDef *huart)
{
  /* Enable DMA clock */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* Configure the DMA */
  /* Configure the DMA handler for Transmission process */
  ex_Uart2DmaTx.Instance                 = DMA1_Stream6;
  ex_Uart2DmaTx.Init.Channel             = DMA_CHANNEL_4;
  ex_Uart2DmaTx.Init.Direction           = DMA_MEMORY_TO_PERIPH;
  ex_Uart2DmaTx.Init.PeriphInc           = DMA_PINC_DISABLE;
  ex_Uart2DmaTx.Init.MemInc              = DMA_MINC_ENABLE;
  ex_Uart2DmaTx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  ex_Uart2DmaTx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
  ex_Uart2DmaTx.Init.Mode                = DMA_NORMAL;
  ex_Uart2DmaTx.Init.Priority            = DMA_PRIORITY_LOW;
  ex_Uart2DmaTx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;
  ex_Uart2DmaTx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
  ex_Uart2DmaTx.Init.MemBurst            = DMA_MBURST_SINGLE;
  ex_Uart2DmaTx.Init.PeriphBurst         = DMA_PBURST_SINGLE;
  HAL_DMA_Init(&ex_Uart2DmaTx);

  /* Associate the initialized DMA handle to the UART handle */
  __HAL_LINKDMA(huart, hdmatx, ex_Uart2DmaTx);

  /* Configure the DMA handler for reception process */
  ex_Uart2DmaRx.Instance                 = DMA1_Stream5;
  ex_Uart2DmaRx.Init.Channel             = DMA_CHANNEL_4;
  ex_Uart2DmaRx.Init.Direction           = DMA_PERIPH_TO_MEMORY;
  ex_Uart2DmaRx.Init.PeriphInc           = DMA_PINC_DISABLE;
  ex_Uart2DmaRx.Init.MemInc              = DMA_MINC_ENABLE;
  ex_Uart2DmaRx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  ex_Uart2DmaRx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
  ex_Uart2DmaRx.Init.Mode                = DMA_CIRCULAR;
  ex_Uart2DmaRx.Init.Priority            = DMA_PRIORITY_HIGH;
  ex_Uart2DmaRx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;
  ex_Uart2DmaRx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
  ex_Uart2DmaRx.Init.MemBurst            = DMA_MBURST_SINGLE;
  ex_Uart2DmaRx.Init.PeriphBurst         = DMA_PBURST_SINGLE;
  HAL_DMA_Init(&ex_Uart2DmaRx);

  /* Associate the initialized DMA handle to the the UART handle */
  __HAL_LINKDMA(huart, hdmarx, ex_Uart2DmaRx);

//  __HAL_DMA_ENABLE_IT(&ex_Uart2DmaRx, DMA_IT_TC);
//
//  /* Configure the NVIC for DMA */
//  /* NVIC configuration for DMA transfer complete interrupt (USARTx_RX) */
//  HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 8, 0);
//  HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);
}




void vUartSendStrings(USART_TypeDef* USARTx, char *pcStrings)
{
    uint32_t uiTime = 0;

    while(*pcStrings != 0)
    {
        uiTime = 10000;
        /* Loop until the end of transmission */
        while(((USARTx->SR & UART_FLAG_TXE) == RESET) && (--uiTime));

        /* Transmit Data */
        USARTx->DR = *pcStrings++;
    }
}

void vUartSendDatas(USART_TypeDef* USARTx, uint8_t *pucDatas, uint32_t uiLength)
{
    uint32_t i = 0, uiTime = 0;

    for(i = 0; i < uiLength; ++i)
    {
        uiTime = 10000;
        /* Loop until the end of transmission */
        while(((USARTx->SR & UART_FLAG_TXE) == RESET) && (--uiTime));

        /* Transmit Data */
        USARTx->DR = *pucDatas++;
    }
}




/**
 * @brief  设置DMA的数据传输长度，并启动DMA传输
 * @param  DMAy_Channelx：   相应DMA的通道
           Length：          传输长度
 * @retval 无
 */
void vDMASetLenthStart(DMA_HandleTypeDef *hdma, uint32_t uiLength)
{
    uint32_t uiTime = 0;

    uiTime = 100000;
    /* Disable the peripheral */
    __HAL_DMA_DISABLE(hdma);
    while(((hdma->Instance->CR & DMA_SxCR_EN) != 0) && (--uiTime));

    /* 必须要清除所有中断状态，才允许使能DMA */
    __HAL_DMA_CLEAR_FLAG(hdma, 0xFFFFFFFF);

    /* Configure DMA Stream data length */
    hdma->Instance->NDTR = uiLength;

    uiTime = 100000;
    /* Enable the peripheral */
    __HAL_DMA_ENABLE(hdma);
    while(((hdma->Instance->CR & DMA_SxCR_EN) == 0) && (--uiTime));
}

/**
 * @brief  设置DMA的数据传输长度，并启动DMA传输
 * @param  DMAy_Channelx：   相应DMA的通道
           Length：          传输长度
 * @retval 无
 */
void vDMAMultiBufferSetLenthStart(DMA_HandleTypeDef *hdma, uint32_t uiLength)
{
    uint32_t uiTime = 0;

    uiTime = 100000;
    /* Disable the peripheral */
    __HAL_DMA_DISABLE(hdma);
    while(((hdma->Instance->CR & DMA_SxCR_EN) != 0) && (--uiTime));

    /* 必须要清除所有中断状态，才允许使能DMA */
    __HAL_DMA_CLEAR_FLAG(hdma, 0xFFFFFFFF);

    /* Configure DMA Stream data length */
    hdma->Instance->NDTR = uiLength;

    /* 切换DMA双缓存指针 */
    hdma->Instance->CR ^= DMA_SxCR_CT;

    uiTime = 100000;
    /* Enable the peripheral */
    __HAL_DMA_ENABLE(hdma);
    while(((hdma->Instance->CR & DMA_SxCR_EN) == 0) && (--uiTime));
}


/**
 * @brief  设置DMA的数据传输长度，并启动DMA传输
 * @param  DMAy_Channelx：   相应DMA的通道
           Length：          传输长度
 * @retval 无
 */
void vDMASetAddrAndLenthStart(DMA_HandleTypeDef *hdma, uint32_t uiSourceAddress, uint32_t uiTargetAddress, uint32_t uiLength)
{
    uint32_t uiTime = 0;

    uiTime = 100000;
    /* Disable the peripheral */
    __HAL_DMA_DISABLE(hdma);
    while(((hdma->Instance->CR & DMA_SxCR_EN) != 0) && (--uiTime));

    /* 必须要清除所有中断状态，才允许使能DMA */
    __HAL_DMA_CLEAR_FLAG(hdma, 0xFFFFFFFF);

    /* Configure DMA Stream data length */
    hdma->Instance->NDTR = uiLength;

    /* Memory to Peripheral */
    if((hdma->Init.Direction) == DMA_MEMORY_TO_PERIPH)
    {
        /* Configure DMA Stream destination address */
        hdma->Instance->PAR = uiTargetAddress;

        /* Configure DMA Stream source address */
        hdma->Instance->M0AR = uiSourceAddress;
    }
    /* Peripheral to Memory */
    else
    {
        /* Configure DMA Stream source address */
        hdma->Instance->PAR = uiSourceAddress;

        /* Configure DMA Stream destination address */
        hdma->Instance->M0AR = uiTargetAddress;
    }

    uiTime = 100000;
    /* Enable the Peripheral */
    __HAL_DMA_ENABLE(hdma);
    /* Wait Enable the peripheral */
    while(((hdma->Instance->CR & DMA_SxCR_EN) == 0) && (--uiTime));
}

void vUartDMASendStrings(USART_TypeDef* USARTx, char *pcStrings)
{
    DMA_HandleTypeDef *hDMAHandle = NULL;
    uint32_t uiDMAISRFlag = 0;

    switch((uint32_t)USARTx)
    {
        case (uint32_t)USART2: hDMAHandle = &ex_Uart2DmaTx; uiDMAISRFlag = DMA_FLAG_TCIF2_6; break;

        default : printf("vUartDMASendStrings channel error.\r\n"); return;
    }

    /* 设置传输地址与长度 */
    vDMASetAddrAndLenthStart(hDMAHandle, (uint32_t)pcStrings, (uint32_t)(&(USARTx->DR)), strlen(pcStrings));

    /* 等待传输完成 */
    while(__HAL_DMA_GET_FLAG(hDMAHandle, uiDMAISRFlag) == RESET);
}

void vUartDMASendDatas(USART_TypeDef* USARTx, uint8_t *pucDatas, uint32_t uiLength)
{
    DMA_HandleTypeDef *hDMAHandle = NULL;
    uint32_t uiDMAISRFlag = 0;

    switch((uint32_t)USARTx)
    {
        case (uint32_t)USART2: hDMAHandle = &ex_Uart2DmaTx; uiDMAISRFlag = DMA_FLAG_TCIF2_6; break;

        default : printf("vUartDMASendDatas channel error.\r\n"); return;
    }

    /* 设置传输地址与长度 */
    vDMASetAddrAndLenthStart(hDMAHandle, (uint32_t)pucDatas, (uint32_t)(&(USARTx->DR)), uiLength);

    /* 等待传输完成 */
    while(__HAL_DMA_GET_FLAG(hDMAHandle, uiDMAISRFlag) == RESET);
}
