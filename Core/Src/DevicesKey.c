#include "stm32f4xx_hal.h"
#include "DevicesKey.h"


void vKeyInit(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable */
    KEY0_RCC_GPIO_CLK_ENABLE();
    KEY1_RCC_GPIO_CLK_ENABLE();
    KEY2_RCC_GPIO_CLK_ENABLE();
    KEY3_RCC_GPIO_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(KEY0_GPIO_Port, KEY0_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(KEY1_GPIO_Port, KEY1_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(KEY2_GPIO_Port, KEY2_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(KEY3_GPIO_Port, KEY3_Pin, GPIO_PIN_SET);

    /*Configure GPIO pin : PtPin */
    GPIO_InitStruct.Pin = KEY0_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(KEY0_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = KEY1_Pin;
    HAL_GPIO_Init(KEY1_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = KEY2_Pin;
    HAL_GPIO_Init(KEY2_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = KEY3_Pin;
    HAL_GPIO_Init(KEY3_GPIO_Port, &GPIO_InitStruct);
}

/*
Return Value:   按键组合序号（头文件里有定义）
Parameters:     void
Description:    读取当前按键状态
*/
uint32_t uiKeyScanf(void)
{
    uint32_t Key = KEY_0 | KEY_1 | KEY_2 | KEY_3;
    uint8_t i = 0;

    for(i = 0; i < 8; ++i)
    {
        if(HAL_GPIO_ReadPin(KEY0_GPIO_Port, KEY0_Pin) != GPIO_PIN_RESET)
            Key &= ~KEY_0;
        if(HAL_GPIO_ReadPin(KEY1_GPIO_Port, KEY1_Pin) != GPIO_PIN_RESET)
            Key &= ~KEY_1;
        if(HAL_GPIO_ReadPin(KEY2_GPIO_Port, KEY2_Pin) != GPIO_PIN_RESET)
            Key &= ~KEY_2;
        if(HAL_GPIO_ReadPin(KEY3_GPIO_Port, KEY3_Pin) != GPIO_PIN_RESET)
            Key &= ~KEY_3;
    }

    return Key;
}
