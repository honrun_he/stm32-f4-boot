/*
 * DevicesSPI.c
 *
 *  Created on: 2020��10��20��
 *      Author: hongyun.he
 */

#include "stm32f4xx_hal.h"
#include "DevicesSPI.h"


SPI_HandleTypeDef ex_Spi1Handle;
SPI_HandleTypeDef ex_Spi2Handle;




void vSPI2Init(void)
{
    GPIO_InitTypeDef  GPIO_InitStruct = {0};

    /* Enable GPIO TX/RX clock */
    __HAL_RCC_GPIOB_CLK_ENABLE();

    /* Enable SPI peripheral Clock */
    __HAL_RCC_SPI2_CLK_ENABLE();


    GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull      = GPIO_NOPULL;
    GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Pin       = GPIO_PIN_12;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* SPI SCK GPIO pin configuration  */
    GPIO_InitStruct.Pin       = GPIO_PIN_13;
    GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull      = GPIO_PULLDOWN;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* SPI MISO GPIO pin configuration  */
    GPIO_InitStruct.Pull      = GPIO_PULLUP;
    GPIO_InitStruct.Pin       = GPIO_PIN_14 | GPIO_PIN_15;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    SPI2_NSS_DISABLE();


    /* Set the SPI parameters */
    ex_Spi2Handle.Instance               = SPI2;
    ex_Spi2Handle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
    ex_Spi2Handle.Init.Direction         = SPI_DIRECTION_2LINES;
    ex_Spi2Handle.Init.CLKPhase          = SPI_PHASE_1EDGE;
    ex_Spi2Handle.Init.CLKPolarity       = SPI_POLARITY_LOW;
    ex_Spi2Handle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
    ex_Spi2Handle.Init.CRCPolynomial     = 7;
    ex_Spi2Handle.Init.DataSize          = SPI_DATASIZE_8BIT;
    ex_Spi2Handle.Init.FirstBit          = SPI_FIRSTBIT_MSB;
    ex_Spi2Handle.Init.NSS               = SPI_NSS_SOFT;
    ex_Spi2Handle.Init.TIMode            = SPI_TIMODE_DISABLE;
    ex_Spi2Handle.Init.Mode              = SPI_MODE_MASTER;
    HAL_SPI_Init(&ex_Spi2Handle);

    __HAL_SPI_ENABLE(&ex_Spi2Handle);
}

int8_t cSPIWriteDatas(SPI_HandleTypeDef *hSPIHand, uint8_t *pBuffer, uint32_t uiLength)
{
    int8_t cError = 0;

    if(HAL_SPI_Transmit(hSPIHand, pBuffer, uiLength, 5000) != HAL_OK)
        cError = -1;

    return cError;
}

int8_t cSPIReadDatas(SPI_HandleTypeDef *hSPIHand, uint8_t *pBuffer, uint32_t uiLength)
{
    int8_t cError = 0;

    if(HAL_SPI_Receive(hSPIHand, pBuffer, uiLength, 5000) != HAL_OK)
        cError = -1;

    return cError;
}
