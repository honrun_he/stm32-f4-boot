#include "stm32f4xx_hal.h"
#include "DevicesFlash.h"
#include "DevicesKey.h"
#include "UserOTA.h"
#include "bootloader.h"





void vJUMPInit(void)
{
    OTAFirmware typeOTAFirmware = {0};

    if((uiKeyScanf() > KEY_2) && ((uiKeyScanf() & (KEY_0 | KEY_1)) == (KEY_0 | KEY_1)))
        return;

    cFlashReadDatas(BOOTLOADER_PARAMETERS_ADDR, (uint8_t *)&typeOTAFirmware, sizeof(typeOTAFirmware));

    if(typeOTAFirmware.jumpSwitch == 0x12345678)
        cJUMPToFirmWare(BOOTLOADER_ADDR);
}

int8_t cJUMPToFirmWare(uint32_t uiAddress)
{
    uint32_t uiJumpAddress = 0, uiHeapData = 0;
    pFunction typeJumpToAPPlication = 0;

    uiHeapData = *(__IO uint32_t*)uiAddress;
    /* Test if user code is programmed starting from address "ApplicationAddress" */
    if (((uiHeapData) & 0x2FFFFFFF ) == uiHeapData)
    {
        /* Jump to user application */
        uiJumpAddress = *(__IO uint32_t*) (uiAddress + 4);
        typeJumpToAPPlication = (pFunction) uiJumpAddress;

//        __disable_irq();

        /* Initialize user application's Stack Pointer */
        __set_MSP(*(__IO uint32_t*) uiAddress);

        /* ��ת��APP���� */
        typeJumpToAPPlication();
    }

    return -1;
}

int8_t cJUMPSetAPPSwitch(uint8_t ucState)
{
    int8_t cError = 0;

    ex_firmwaeMCUAPP.jumpSwitch = (ucState == DISABLE) ? 0x12345678 : 0;

    cError = cOTAParametersWrite();

    return cError;
}


int8_t cJUMPSetBootloaderSwitch(uint8_t ucState)
{
    int8_t cError = 0;

    ex_firmwaeMCUBootloader.jumpSwitch = (ucState == DISABLE) ? 0x12345678 : 0;

    cError = cOTAParametersWrite();

    return cError;
}
