/*
 * UserOTA.c
 *
 *  Created on: 2020年9月15日
 *      Author: Honrun
 */

#include "stdio.h"
#include "stdint.h"
#include "string.h"
#include "DevicesFlash.h"
#include "DevicesSPIFlash.h"
#include "DevicesCRC.h"
#include "DevicesLed.h"
#include "UserOTA.h"

OTAFirmware ex_firmwaeMCUAPP = {0};
OTAFirmware ex_firmwaeMCUAPPUpdate = {0};
OTAFirmware ex_firmwaeMCUBootloader = {0};
OTAFirmware ex_firmwaeMCUBootloaderUpdate = {0};


static uint8_t st_ucOTAParameters[512];


int8_t cOTAParametersRead(void)
{
    int8_t cError = 0;

    cError |= cFlashReadDatas(BOOTLOADER_PARAMETERS_ADDR, (uint8_t *)(&ex_firmwaeMCUBootloader), sizeof(OTAFirmware));
    cError |= cFlashReadDatas(BOOTLOADER_PARAMETERS_ADDR + sizeof(OTAFirmware), (uint8_t *)(&ex_firmwaeMCUBootloaderUpdate), sizeof(OTAFirmware));

    cError |= cFlashReadDatas(APP_PARAMETERS_ADDR, (uint8_t *)(&ex_firmwaeMCUAPP), sizeof(OTAFirmware));
    cError |= cFlashReadDatas(APP_PARAMETERS_ADDR + sizeof(OTAFirmware), (uint8_t *)(&ex_firmwaeMCUAPPUpdate), sizeof(OTAFirmware));

    ex_firmwaeMCUBootloader.startAddr = BOOTLOADER_ADDR;
    ex_firmwaeMCUAPP.startAddr = APP_ADDR;

    ex_firmwaeMCUBootloaderUpdate.startAddr   = (ex_firmwaeMCUBootloaderUpdate.startAddr   > FLASH_USER_MAX_ADDR)  ? BOOTLOADER_BACK_ADDR : ex_firmwaeMCUBootloaderUpdate.startAddr;
    ex_firmwaeMCUAPPUpdate.startAddr          = (ex_firmwaeMCUAPPUpdate.startAddr          > FLASH_USER_MAX_ADDR)  ? APP_BACK_ADDR        : ex_firmwaeMCUAPPUpdate.startAddr;

    ex_firmwaeMCUBootloader.totalLength       = (ex_firmwaeMCUBootloader.totalLength       > OTA_TOTAL_LENGTH_MAX) ? OTA_TOTAL_LENGTH_MIN : ex_firmwaeMCUBootloader.totalLength;
    ex_firmwaeMCUBootloaderUpdate.totalLength = (ex_firmwaeMCUBootloaderUpdate.totalLength > OTA_TOTAL_LENGTH_MAX) ? OTA_TOTAL_LENGTH_MIN : ex_firmwaeMCUBootloaderUpdate.totalLength;
    ex_firmwaeMCUAPP.totalLength              = (ex_firmwaeMCUAPP.totalLength              > OTA_TOTAL_LENGTH_MAX) ? OTA_TOTAL_LENGTH_MIN : ex_firmwaeMCUAPP.totalLength;
    ex_firmwaeMCUAPPUpdate.totalLength        = (ex_firmwaeMCUAPPUpdate.totalLength        > OTA_TOTAL_LENGTH_MAX) ? OTA_TOTAL_LENGTH_MIN : ex_firmwaeMCUAPPUpdate.totalLength;

    ex_firmwaeMCUBootloader.version[sizeof(ex_firmwaeMCUAPPUpdate.version) - 1]         = 0;
    ex_firmwaeMCUBootloaderUpdate.version[sizeof(ex_firmwaeMCUAPPUpdate.version) - 1]   = 0;
    ex_firmwaeMCUAPP.version[sizeof(ex_firmwaeMCUAPPUpdate.version) - 1]                = 0;
    ex_firmwaeMCUAPPUpdate.version[sizeof(ex_firmwaeMCUAPPUpdate.version) - 1]          = 0;

    return cError;
}

int8_t cOTAParametersWrite(void)
{
    int8_t cError = 0;

    cError |= cFlashWriteDatas(BOOTLOADER_PARAMETERS_ADDR, (uint8_t *)(&ex_firmwaeMCUBootloader), sizeof(OTAFirmware));
    cError |= cFlashWriteDatas(BOOTLOADER_PARAMETERS_ADDR + sizeof(OTAFirmware), (uint8_t *)(&ex_firmwaeMCUBootloaderUpdate), sizeof(OTAFirmware));

    cError |= cFlashWriteDatas(APP_PARAMETERS_ADDR, (uint8_t *)(&ex_firmwaeMCUAPP), sizeof(OTAFirmware));
    cError |= cFlashWriteDatas(APP_PARAMETERS_ADDR + sizeof(OTAFirmware), (uint8_t *)(&ex_firmwaeMCUAPPUpdate), sizeof(OTAFirmware));

    return cError;
}

int8_t cOTAUpdate(OTAFirmware *pSrc, OTAFirmware *pDst)
{
    int8_t cError = 0;
    uint32_t i = 0, srcAdddrNow = 0, dstAdddrNow = 0, uiLength = 0;

//    if(pSrc->totalCRC != pDst->totalCRC)
    {
        printf("APP update start...\r\n");
        printf("APP file size: %d\r\n", (int)(pSrc->totalLength));

        /* CRC比较 */
        if(uiOTAFirmwaeCRCUpdate(pSrc) != pSrc->totalCRC)
            return -1;

        /* CRC比较 */
        if(uiOTAFirmwaeCRCUpdate(pDst) == pSrc->totalCRC)
            return 0;

        srcAdddrNow = pSrc->startAddr;
        dstAdddrNow = pDst->startAddr;

        /* 升级覆盖 */
        for(i = 0; i < pSrc->totalLength; i += uiLength)
        {
            uiLength = ((pSrc->totalLength - i) > sizeof(st_ucOTAParameters)) ? sizeof(st_ucOTAParameters) : (pSrc->totalLength - i);

            if((pSrc->startAddr >= FLASH_ZERO_ADDR) && (pSrc->startAddr < FLASH_USER_MAX_ADDR))
                cError |= cFlashReadDatas(srcAdddrNow, st_ucOTAParameters, uiLength);
            else
                cError |= cSPIFlashReadDatas(srcAdddrNow, st_ucOTAParameters, uiLength);

            cError |= cFlashWriteDatas(dstAdddrNow, st_ucOTAParameters, uiLength);

            srcAdddrNow += uiLength;
            dstAdddrNow += uiLength;

            printf("APP update: %d%%\r\n", (int)(100 * i / pSrc->totalLength));
            LED_REVESAL();
        }

        /* 更新目标参数 */
        pDst->totalLength = pSrc->totalLength;
        pDst->totalCRC = pSrc->totalCRC;
        memcpy(pDst->version, pSrc->version, 16);

        cError |= cOTAParametersWrite();

        printf("APP update stop.\r\n");
    }

    return cError;
}

uint32_t uiOTAFirmwaeCRCUpdate(OTAFirmware *pSrc)
{
    uint32_t i = 0, srcAdddrNow = 0, uiCRCNow = 0xFFFFFFFF, uiLength = 0;

    srcAdddrNow = pSrc->startAddr;

    /* CRC计算 */
    for(i = 0; i < pSrc->totalLength; i += uiLength)
    {
        uiLength = ((pSrc->totalLength - i) > sizeof(st_ucOTAParameters)) ? sizeof(st_ucOTAParameters) : (pSrc->totalLength - i);

        if((pSrc->startAddr >= FLASH_ZERO_ADDR) && (pSrc->startAddr < FLASH_USER_MAX_ADDR))
            cFlashReadDatas(srcAdddrNow, st_ucOTAParameters, uiLength);
        else
            cSPIFlashReadDatas(srcAdddrNow, st_ucOTAParameters, uiLength);

        uiCRCNow = uiCRC32_MPEG2(&uiCRCNow, st_ucOTAParameters, uiLength);

        srcAdddrNow += uiLength;
    }

    return uiCRCNow;
}
