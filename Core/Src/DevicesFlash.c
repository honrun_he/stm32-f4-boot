#include "stm32f4xx_hal.h"
#include "stdio.h"
#include "stdint.h"
#include "DevicesFlash.h"


int8_t cFlashWriteDatas(uint32_t uiAddress, uint8_t *pucBuff, uint32_t uiLength)
{
    FLASH_EraseInitTypeDef EraseInitStruct = {0};
    uint32_t i = 0, SECTORError = 0, uiCnt = 0;
    uint16_t *pusDataAddress = (uint16_t *)pucBuff;
    int8_t cError = 0;

    /* Fill EraseInit structure*/
    EraseInitStruct.TypeErase     = FLASH_TYPEERASE_SECTORS;
    EraseInitStruct.VoltageRange  = FLASH_VOLTAGE_RANGE_3;
    EraseInitStruct.NbSectors     = 1;

    /* Unlock the Flash to enable the flash control register access *************/
    HAL_FLASH_Unlock();

    uiLength >>= 1;
    /* Write Flash */
    for(i = 0; i < uiLength; ++i)
    {
        if(uiAddress >= FLASH_USER_MAX_ADDR)
        {
            cError |= 1;
            break;
        }

        if(cFlashGetErasePosition(uiAddress) != -1)
        {
            EraseInitStruct.Sector = cFlashGetErasePosition(uiAddress);

            uiCnt = 10000;
            while((HAL_FLASHEx_Erase(&EraseInitStruct, &SECTORError) != HAL_OK) && (--uiCnt));

            if(uiCnt == 0)
            {
//                printf("cFlashWriteDatas HAL_FLASHEx_Erase addr: 0x%08X error: %d!\r\n", (unsigned int)uiAddress, (int)SECTORError);
                cError |= 2;
                break;
            }
        }

        if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, uiAddress, pusDataAddress[i]) != HAL_OK)
        {
//            printf("cFlashWriteDatas HAL_FLASH_Program addr: 0x%08X error!\r\n", (unsigned int)uiAddress);
            cError |= 4;
            break;
        }
        uiAddress += 2;
    }

    /* Lock the Flash to disable the flash control register access (recommended
       to protect the FLASH memory against possible unwanted operation) *********/
    HAL_FLASH_Lock();

    return cError;
}

int8_t cFlashReadDatas(uint32_t uiAddress, uint8_t * pucBuff, uint32_t uiLength)
{
    uint32_t i = 0;
    uint16_t *pusDataAddress = (uint16_t *)pucBuff;

    /* 不能以奇数地址进行操作 */
    if(uiAddress & 1)
        return -1;

    if((uiAddress + uiLength) >= FLASH_USER_MAX_ADDR)
        return -2;

    uiLength >>= 1;
    /* Read Flash */
    for(i = 0; i < uiLength; ++i)
    {
        pusDataAddress[i] = *(__IO uint16_t*) uiAddress;
        uiAddress += 2;
    }

    return 0;
}

int8_t cFlashGetErasePosition(uint32_t Address)
{
    switch(Address)
    {
        case ADDR_FLASH_SECTOR_0: return FLASH_SECTOR_0;
        case ADDR_FLASH_SECTOR_1: return FLASH_SECTOR_1;
        case ADDR_FLASH_SECTOR_2: return FLASH_SECTOR_2;
        case ADDR_FLASH_SECTOR_3: return FLASH_SECTOR_3;
        case ADDR_FLASH_SECTOR_4: return FLASH_SECTOR_4;
        case ADDR_FLASH_SECTOR_5: return FLASH_SECTOR_5;
        case ADDR_FLASH_SECTOR_6: return FLASH_SECTOR_6;
        case ADDR_FLASH_SECTOR_7: return FLASH_SECTOR_7;
        case ADDR_FLASH_SECTOR_8: return FLASH_SECTOR_8;
        case ADDR_FLASH_SECTOR_9: return FLASH_SECTOR_9;
        case ADDR_FLASH_SECTOR_10: return FLASH_SECTOR_10;
        case ADDR_FLASH_SECTOR_11: return FLASH_SECTOR_11;

        default: break;
    }

    return -1;
}



uint32_t uiFlashGetPositionSize(uint32_t Address)
{
    switch(Address)
    {
        case ADDR_FLASH_SECTOR_0: return 16 * 1024;
        case ADDR_FLASH_SECTOR_1: return 16 * 1024;
        case ADDR_FLASH_SECTOR_2: return 16 * 1024;
        case ADDR_FLASH_SECTOR_3: return 16 * 1024;
        case ADDR_FLASH_SECTOR_4: return 64 * 1024;
        case ADDR_FLASH_SECTOR_5: return 128 * 1024;
        case ADDR_FLASH_SECTOR_6: return 128 * 1024;
        case ADDR_FLASH_SECTOR_7: return 128 * 1024;
        case ADDR_FLASH_SECTOR_8: return 128 * 1024;
        case ADDR_FLASH_SECTOR_9: return 128 * 1024;
        case ADDR_FLASH_SECTOR_10: return 128 * 1024;
        case ADDR_FLASH_SECTOR_11: return 128 * 1024;

        default: break;
    }

    return 0;
}

int8_t cFlashReadProtection(uint8_t NewState)
{
    FLASH_OBProgramInitTypeDef OBInit = {0};

    /* Allow Access to option bytes sector */
    HAL_FLASH_OB_Unlock();

    /* Allow Access to Flash control registers and user Flash */
    HAL_FLASH_Unlock();

    /* Disable FLASH_WRP_SECTORS write protection */
    OBInit.OptionType = OPTIONBYTE_RDP;

    /* 千万千万不要选择等级2 */
    if(NewState != DISABLE)
        OBInit.RDPLevel = OB_RDP_LEVEL_1;
    else
        OBInit.RDPLevel = OB_RDP_LEVEL_0;

    HAL_FLASHEx_OBProgram(&OBInit);

    /* 使设置生效 */
    HAL_FLASH_OB_Launch();

    /* Prevent Access to option bytes sector */
    HAL_FLASH_OB_Lock();

    /* Disable the Flash option control register access (recommended to protect
    the option Bytes against possible unwanted operations) */
    HAL_FLASH_Lock();

    return 0;
}
