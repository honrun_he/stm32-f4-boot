#include "stm32f4xx_hal.h"
#include "DevicesLed.h"





void vLedInit(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable */
    LED_RCC_GPIO_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);

    /*Configure GPIO pin : PtPin */
    GPIO_InitStruct.Pin = LED_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(LED_GPIO_Port, &GPIO_InitStruct);

    LED_OPEN();
}

void vLedOpen(uint8_t ucNumber)
{
    if(ucNumber & LED_0)
        HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
}

void vLedClose(uint8_t ucNumber)
{
    if(ucNumber & LED_0)
        HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
}

void vLedRevesal(uint8_t ucNumber)
{
    if(ucNumber & LED_0)
        HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
}
