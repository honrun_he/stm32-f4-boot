/*
 * File:   UserDelay.c
 * Author: SuiFeng
 * brief:  STM32F4系列延时函数，延时函数的调用，请先调用初始化函数 vDelayInit 进行初始化，然后使用头文件里定义的宏： vDelayS、 vDelayMs、 vDelayUs
 * Created on 2019年3月13日, 下午2:04
 */

#include "stm32f4xx_hal.h"
#include "stdint.h"
#include "DevicesDelay.h"


/*
 * Return:      void
 * Parameters:  void
 * Description: SysTick初始化
 */
void vSysTickDelayInit(void)
{
    SysTick->CTRL = SysTick_CTRL_TICKINT_Msk;                           /* TICKINT置1-单次触发、CLKSOURCE清零-SysTick时钟选择为系统时钟8分频 */
}

/*
 * Return:      void
 * Parameters:  Time: 延时时间
 * Description: 秒延时
 */
void vSysTickDelayS(uint32_t uiTime)
{
    uint32_t i = 0u;

    for(i = 0u; i < uiTime; ++i)
    {
        SysTick->LOAD  = SystemCoreClock / 8u - 1u;                     /* Set the SysTick Counter Load */
        SysTick->VAL   = 0u;                                            /* Clear the SysTick Counter Value */
        SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;                       /* Enable SysTick Timer */

        while((SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) == 0u);      /* 当VAL计数寄存器递减到0时，COUNTFLAG自动置1（读取后自动清零） */
    }
}

/*
 * Return:      void
 * Parameters:  Time: 延时时间（范围：1~1864，系统时钟为72M时）
 * Description: 毫秒延时
 */
void vSysTickDelayMs(float fTime)
{
    if(fTime < 0.01f)
        return;

    SysTick->LOAD  = SystemCoreClock / 8000u * fTime - 1u;         /* Set the SysTick Counter Load */
    SysTick->VAL   = 0u;                                            /* Clear the SysTick Counter Value */
    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;                       /* Enable SysTick Timer */

    while((SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) == 0u);      /* 当VAL计数寄存器递减到0时，COUNTFLAG自动置1（读取后自动清零） */
}

/*
 * Return:      void
 * Parameters:  Time: 延时时间（范围：1~1864135，系统时钟为72M时）
 * Description: 微秒延时
 */
void vSysTickDelayUs(float fTime)
{
    if(fTime < 0.1f)
        return;

    SysTick->LOAD  = SystemCoreClock / 8000000u * fTime - 1u;      /* Set the SysTick Counter Load */
    SysTick->VAL   = 0u;                                            /* Clear the SysTick Counter Value */
    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;                       /* Enable SysTick Timer */

    while((SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) == 0u);      /* 当VAL计数寄存器递减到0时，COUNTFLAG自动置1（读取后自动清零） */
}

/*
 * Return:      void
 * Parameters:  void
 * Description: DWT初始化
 */
void vDWTDelayInit(void)
{
    DEM_CR |= DEM_CR_TRCENA;
    DWT_CYCCNT = 0;
    DWT_CR |= DWT_CR_CYCCNTENA;
}

/*
 * Return:      void
 * Parameters:  Time: 延时时间（范围：1~59，系统时钟为72M时）
 * Description: 秒延时
 */
void vDWTDelayS(uint32_t uiTime)
{
    uint32_t i = 0u;

    if(uiTime < 1u)
        return;

    for(i = 0u; i < uiTime; ++i)
    {
        vDWTDelayMs(1000);
    }
}

/*
 * Return:      void
 * Parameters:  Time: 延时时间（范围：1~59652，系统时钟为72M时）
 * Description: 毫秒延时
 */
void vDWTDelayMs(float fTime)
{
    volatile uint32_t uiTimeStop = 0u, uiTimeStart = 0;

    if(fTime < 0.01f)
        return;

    uiTimeStart = DWT_CYCCNT;
    uiTimeStop  = (uint32_t)((SystemCoreClock / 1000u) * fTime) + uiTimeStart;

    if(uiTimeStop >= uiTimeStart)
        while((DWT_CYCCNT > uiTimeStart) && (DWT_CYCCNT < uiTimeStop));
    else
        while(!((DWT_CYCCNT > uiTimeStop) && (DWT_CYCCNT < uiTimeStart)));
}

/*
 * Return:      void
 * Parameters:  Time: 延时时间（范围：1~59652323，系统时钟为72M时）
 * Description: 微秒延时
 */
void vDWTDelayUs(float fTime)
{
    volatile uint32_t uiTimeStop = 0u, uiTimeStart = 0;

    if(fTime < 0.1f)
        return;

    uiTimeStart = DWT_CYCCNT;
    uiTimeStop  = (uint32_t)((SystemCoreClock / 1000000u) * fTime) + uiTimeStart;

    if(uiTimeStop >= uiTimeStart)
        while((DWT_CYCCNT > uiTimeStart) && (DWT_CYCCNT < uiTimeStop));
    else
        while(!((DWT_CYCCNT > uiTimeStop) && (DWT_CYCCNT < uiTimeStart)));
}
